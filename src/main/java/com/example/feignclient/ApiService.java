package com.example.feignclient;

import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


public interface ApiService  {
    @GetMapping("/rest/v2/all")
    List<Object> getCountries();
}
