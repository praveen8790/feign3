package com.example.feignclient;

import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "api-service",url = "https://restcountries.eu/")
public interface ApiServiceProxy extends ApiService {
}
