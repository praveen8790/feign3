package com.example.feignclient;



import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@EnableFeignClients(basePackageClasses = ApiServiceProxy.class)
public class ApiController implements ApiServiceProxy {

    public ApiServiceProxy proxy;

    public ApiController(@Qualifier("com.example.feignclient.ApiServiceProxy") ApiServiceProxy proxy) {
        this.proxy = proxy;
    }

    @Override
    @GetMapping("/rest/v2/all")
    public List<Object> getCountries() {
        return this.proxy.getCountries();
    }
}
